'use strict'
let url = "https://reqres.in/api/users?per_page=12";

fetch(url)
  .then((response) => {
   
    return response.json();
  })
  .then((response) => {
  
    return printResults(response);
  })
  .catch((e) => {
    console.log(e);
    alert("url не правильный!");
  });

function printResults(response) {
  console.log(response);
  console.log(response.data);

  console.log("-----------");
  console.log("Пункт №1:");
  console.log("-----------");

  let results = response.data;


  results.forEach((element) => console.log(element.last_name));

  console.log("-----------");
  console.log("Пункт №2:");
  console.log("-----------");

  for (let i = 0; i < results.length; i++) {
    console.log(results[i].last_name[0]);

    if (results[i].last_name[0] === "F") {
      console.log(results[i]);
    }
  }

  console.log("-----------");
  console.log("Пункт №3:");
  console.log("-----------");

  let pynkt3result = "Наша база содержит данные следующих пользователей: ";
  for (let i = 0; i < results.length; i++) {
  
    pynkt3result +=
      results[i].first_name +
      " " +
      results[i].last_name +
      (i !== results.length - 1 ? ", " : ".");
  }
  console.log(pynkt3result);

  console.log(
    results.reduce(
      (acc, item) => acc + item.first_name + " " + item.last_name + ", ",
      "Наша база содержит данные следующих пользователей: "
    )
  );

  console.log("-----------");
  console.log("Пункт №4:");
  console.log("-----------");

  console.log(Object.keys(results[0]));
}
